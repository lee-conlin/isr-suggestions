# Player manned weapons

[TOC]

The idea behind this suggestion is that there should be more variety in the options available for weapons that require a crew-member to operate.

Currently the only such weapon available is the turret.

## Other turrets

- Top/Bottom turrets with stairs up/down and a 360-degree field of fire.
- Missile control turrets - allows the operator to lock targets and fire missiles.

## Other opportunities for player assisted weapons

Torpedo launchers, being a larger ship weapon, should require a "Weapons station" that is manned by another player. The weapons station would lock the targets for the torpedos and fire them independently of the pilot.

Likewise, the ship railguns should be fired by the weapons station too.

Basically, any "capital ship" type weapon should require crew to fire.  At this level the captain would issue orders, the pilot flies, the weapons officer fires the big guns.

## The benefit

This will encourage crews by allowing much more powerful warships and better offensive/defensive capabilities when utilising crew operated weapons. 