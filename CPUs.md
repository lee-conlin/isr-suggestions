[TOC]

The aim of this document is to be a living document that I will update as I get constructive feedback.



# CPUs : The basic Idea

The idea I'm putting forward expands on the current idea of CPU limits but ties it into the rest of the ship in a more immersive way.

**Note:** All numbers quoted can be up to discussion later - they're just placeholders for now to illustrate the idea. Don't get hung up on the numbers!

My suggestion is to do away with the "Weapons CPU" blocks and make use of the computer cores already in the game - the ones that take cartridges.

Essentially it would allow you to dedicate computer cores to different tasks around the ship. A single "core" could provide CPU for weapons, shields and defenses but you could assign multiple cores with some soft-limits.

Each "Core" should have a CPU value that is it's rated CPU load/capacity. If a player assigns more devices to that core than it's capacity allows, it **can** run up to 200% of its capacity but it will generate heat and take damage from doing so. The more above 100% it gets the more heat/damamge it takes.

Heatsinks could be applied in this scenario to allow cores to operate at higher than rated capacity. This introduces a consumable cost to running more things on a core than it can normally handle.

In addition, the more work a core is doing, the more power it will draw. Cores that are assigned to "CPU" also cannot be assigned to cartridges. 

# Limiting gun-walls without nerfing big ships

This area is one of much debate but my thoughts on this matter are that the limits should be imposed by the power/resource requirements of the cores and some form of diminishing returns.

For example, if 1 core assigned to CPU generates 50, 2 cores might produce a total of 95, 3 cores a total of 140. Remember that with heatsinks these numbers could effectively be doubled, but with the diminishing returns there will be a natural "sweet spot" where putting any more in is wasteful.

Since you can currently get two-cores in a single block square, whatever numbers end up being assigned here would need to be thought out to still allow for small fighter-style ships with 2-4 cores in the back and a few weapons... as well as allowing for much bigger ships to still attain a decent CPU count before diminishing returns makes it not worth the space/power.

Something like this might work...

and remember - *All numbers quoted can be up to discussion later - they're just placeholders for now to illustrate the idea. Don't get hung up on the numbers!*

| # Cores | CPU for new core | Total CPU | Total CPU Overclocked (Requires heatsinks) | # blocks of space |
| ------- | ---------------- | --------- | ------------------------------------------ | ----------------- |
| 1       | 150              | 150       | 300                                        | 0.5               |
| 2       | 130              | 280       | 561                                        | 1                 |
| 3       | 113              | 394       | 788                                        | 1.5               |
| 4       | 99               | 492       | 985                                        | 2                 |
| 5       | 86               | 578       | 1156                                       | 2.5               |
| 6       | 75               | 653       | 1306                                       | 3                 |
| 7       | 65               | 718       | 1435                                       | 3.5               |
| 8       | 56               | 774       | 1548                                       | 4                 |
| 9       | 49               | 823       | 1646                                       | 4.5               |
| 10      | 43               | 866       | 1731                                       | 5                 |
| 11      | 37               | 903       | 1806                                       | 5.5               |
| 12      | 32               | 935       | 1870                                       | 6                 |
| 13      | 28               | 963       | 1926                                       | 6.5               |
| 14      | 24               | 987       | 1975                                       | 7                 |
| 15      | 21               | 1009      | 2017                                       | 7.5               |
| 16      | 18               | 1027      | 2054                                       | 8                 |

Assuming that bigger ships will have more devices, including shields, armor generators, etc. This could work to effectively limit gun-walls while not stifling player design.

# Balancing ships of the same size with different purposes

Several people have mentioned that a mining ship should not be able to stand up to a warship of the same size. I think this is true and to that end I would propose that CPU also be required for operating other equipment on the ship.

- Extractors
- Refineries
- Assemblers
- 3d printers
- Teleporters (people and cargo)
- Cockpit/warp (the new bigger bridges could use more CPU since they also have holotables)
- Holotables
- Rift generators
- Shields
- Armour generators
- Drone bays
- Strip miners
- Sensors
- etc.

Basically, anything that might require computer power.

**However!!!** - I think that these devices should require, in general, less CPU than weapons. For example, if a pilot gun uses 50 CPU an extractor might use 15.

# Existing functionality (Cartridges)

The cores would also retain the existing functionality of expansion cartridges as well. But they can't be used for an expansion cartridge and for CPU at the same time.

This will mean that ships will need more than their CPU requirement in cores in order to use expansion cartridges.

# Player manned weapons

Player manned weapons such as turrets currently use no CPU. I think this should change in the new system proposed above. If mining and refining devices use CPU then so should turrets.

That said however, I believe that player manned weapons should require much less CPU than pilot weapons since they also require crew members to operate.

I also think that there should be more player manned weapons, but that's a topic for [another suggestion](Player manned weapons.md).

